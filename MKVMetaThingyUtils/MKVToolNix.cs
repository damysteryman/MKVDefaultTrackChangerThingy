﻿/*
 * Copyright (C) 2016-2020  damysteryman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.IO;

namespace MKVMetaThingyUtils
{
    public static class MKVToolNix
    {
        /// <summary>
        /// Name of mkvmerge executable
        /// </summary>
        public const string mkvmergeExe = "mkvmerge";

        /// <summary>
        /// name of mkvpropedit executable
        /// </summary>
        public const string mkvpropeditExe = "mkvpropedit";

        /// <summary>
        /// MKVToolNix Homepage url
        /// </summary>
        public const string urlHome = "https://mkvtoolnix.download/";

        /// <summary>
        /// MKVToolNix Download page URL
        /// </summary>
        public const string urlDownload = "http://www.fosshub.com/MKVToolNix.html";

        /// <summary>
        /// The default output returned from mkvpropedit when everything is completed ok
        /// </summary>
        public static readonly string mkvpropedit_OK = $"The file is being analyzed.{Environment.NewLine}The changes are written to the file.{Environment.NewLine}Done.{Environment.NewLine}";
        
        /// <summary>
        /// The output returned from mkvpropedit when it was unable to open a file
        /// </summary>
        public static readonly string mkvpropedit_ERROR_CannotOpenFile = $"The file is being analyzed.{Environment.NewLine}Error: This file could not be opened or parsed.{Environment.NewLine}";
        
        /// <summary>
        /// Generic template string used for the "track not found" error strings
        /// </summary>
        private const string mkvpropedit_ERROR_MissingTrack = "Error: No track corresponding to the edit specification '";
        
        /// <summary>
        /// The output returned from mkvpropedit when a specified video track is not found/missing
        /// </summary>
        public const string mkvPropedit_ERROR_MissingVideoTrack = mkvpropedit_ERROR_MissingTrack + "v";
        
        /// <summary>
        /// The output returned from mkvpropedit when a specified audio track is not found/missing
        /// </summary>
        public const string mkvPropedit_ERROR_MissingAudioTrack = mkvpropedit_ERROR_MissingTrack + "a";
        
        /// <summary>
        /// The output returned from mkvpropedit when a specified subtitle track is not found/missing
        /// </summary>
        public const string mkvPropedit_ERROR_MissingSubTrack = mkvpropedit_ERROR_MissingTrack + "s";

        /// <summary>
        /// String representing the Video track type
        /// </summary>
        public const string TRACK_VIDEO = "video";

        /// <summary>
        /// String representing the Audio track type
        /// </summary>
        public const string TRACK_AUDIO = "audio";

        /// <summary>
        /// String representing the Subtitle track type
        /// </summary>
        public const string TRACK_SUB = "subtitles";

        /// <summary>
        /// Method to get the paths of certain MKVToolNix Executables
        /// </summary>
        /// <param name="exeName">The specific MKVToolNix-related executable to look for</param>
        /// <returns>Full path of the executable</returns>
        public static string ExeGetPath(string exeName)
        {
            // Array of paths to look in for specified mkvtoolnix-related executable
            string[] paths = { 
                                $@"C:\Program Files\MKVToolNix\{exeName}.exe",
                                $@"C:\Program Files (x86)\MKVToolNix\{exeName}.exe",
                                $@"mkvtoolnix\{exeName}.exe",
                                $"{exeName}.exe",
                                $"/usr/bin/{exeName}",
                                exeName
                             };

            // Return the first path where exe is found
            for (int i = 0; i < paths.Length; i++)
                if (File.Exists(paths[i]))
                    return paths[i];
            
            // If not found just return null string
            return null;
        }

        /// <summary>
        /// Method to check if MKVToolNix is installed or not
        /// </summary>
        /// <returns>true if MKVToolNix is installed, false if it is not.</returns>
        public static bool IsInstalled()
        {
            bool isInstalled = true;    // default assign to true

            if (ExeGetPath(mkvmergeExe) == null || ExeGetPath(mkvpropeditExe) == null)
                isInstalled = false;    // change to false if mkvmerge or mkvpropedit could not be found

            return isInstalled;         // return the result
        }
    }
}
