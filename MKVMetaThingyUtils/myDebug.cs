﻿/*
 * Copyright (C) 2016-2020  damysteryman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Diagnostics;

namespace MKVMetaThingyUtils
{
    /// <summary>
    /// Class with debug-related utilities
    /// </summary>
    public static class myDebug
    {
        // http://stackoverflow.com/questions/171970/how-can-i-find-the-method-that-called-the-current-method
        // http://stackoverflow.com/questions/5080477/c-sharp-debug-only-code-that-should-run-only-when-turned-on
        // https://msdn.microsoft.com/en-us/library/mt653988.aspx
        /// <summary>
        /// Custom wrapper for Debug.WriteLine(), but adds extra information to the output,
        /// such as a timestamp and the method that called it, along with the string that the calling method pass to it.
        /// </summary>
        /// <param name="line">The string to be printed to the debug console.</param>
        /// <param name="memberName">The name of the method that called this one</param>
        [ConditionalAttribute("DEBUG")]     // Only used in Debug build of application, not in Release build
        public static void WriteLine(string line, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            // Get name of calling class from StackFrame
            // Probably not the best way to do things, but since this code only runs in Debug build, it should be OK.
            string className = new StackFrame(1).GetMethod().ReflectedType.Name;

            // Print supplied string line with timestamp and calling class and method names.
            Debug.WriteLine("[" + DateTime.Now.ToString() + "]\t[" + className + "." + memberName + "()]\t" + line);
        }
    }
}
