﻿/*
 * Copyright (C) 2016-2020  damysteryman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using Newtonsoft.Json;

namespace MKVMetaThingyUtils
{
    public static class Utils
    {
        // taken from http://www.dotnetperls.com/redirectstandardoutput
        /// <summary>
        /// Runs a command line process with arguments, redirecting stdout and returning it as a string
        /// </summary>
        /// <param name="exeName">Name of the Executable program</param>
        /// <param name="args">Command line paguments to pass to the program</param>
        /// <returns>stdout from program</returns>
        public static string RunProcess(string exeName, string args)
        {
            myDebug.WriteLine(exeName + " " + args);
            // Setup config for process
            ProcessStartInfo start = new ProcessStartInfo
            {
                FileName = $@"{exeName}",
                Arguments = args,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true
            };

            // Run process and return its output
            using (Process process = Process.Start(start))
                using (StreamReader reader = process.StandardOutput)
                    return reader.ReadToEnd();
        }

        /// <summary>
        /// Opens a URL in the user's default browser program
        ///
        /// TAKEN FROM: https://brockallen.com/2016/09/24/process-start-for-urls-on-net-core/
        /// </summary>
        /// <param name="url">The full URL to load</param>
        public static void OpenURL(string url)
        {
            try
            {
                Process.Start(url);
            }
            catch
            {
                // hack because of this: https://github.com/dotnet/corefx/issues/10361
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    url = url.Replace("&", "^&");
                    Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                    Process.Start("xdg-open", url);
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                    Process.Start("open", url);
                else
                    throw new Exception($"Unknown OS Platform. Manually open link in browser: [{url}]");
            }
        }

        /// <summary>
        /// Deserializes a JSON text string and returns a dynamic object version
        /// </summary>
        /// <param name="jsonString">The actual JSON formatted text</param>
        /// <returns>dynamic object with data from JSON</returns>
        public static dynamic GetJsonObject(string jsonString) => JsonConvert.DeserializeObject(jsonString);
    }
}
