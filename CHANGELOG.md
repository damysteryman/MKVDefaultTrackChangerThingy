# CHANGELOG

# v2.0

Remade UI in GTK/Glade, port to .NET Core 3.1

## MKVDefaultTrackChangerThingy
frmMain.cs
- NEW: Separated UI and core code, placed UI into frmMain class
- NEW: Rewrote UI to use GTKSharp + Glade instead of WinForms
- UPDATE: Port to .NET Core 3.1

UI/frmMain.glade
- NEW: Rewrote UI to use GTKSharp + Glade instead of WinForms

## MKVDefaultTrackChanger:
DefaultTrackChanger.cs
- NEW: Separated UI and core code, placed core into DefaultTrackChanger class
- UPDATE: Port to .NET Core 3.1

## MKVMetaThingyUtils
Utils.cs
- UPDATE: Port to .NET Core 3.1
- UPDATE: removed unused code
