/*
 * Copyright (C) 2020  damysteryman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Gtk;
using MKVDefaultTrackChanger;

namespace MKVDefaultTrackChangerThingy
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Application.Init();

            var app = new Application("org.MKVDefaultTrackChangerThingy.MKVDefaultTrackChangerThingy", GLib.ApplicationFlags.None);
            app.Register(GLib.Cancellable.Current);

            DefaultTrackChanger dtc = new DefaultTrackChanger();
            var win = new frmMain(dtc);
            app.AddWindow(win);

            win.Show();
            Application.Run();
        }
    }
}
