/*
 * Copyright (C) 2020  damysteryman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using MKVDefaultTrackChanger;
using MKVMetaThingyUtils;

namespace MKVDefaultTrackChangerThingy
{
    class frmMain : Window
    {
        #region vars
        private DefaultTrackChanger dtc;
        private int fileLoadSaveCount = 0;
        #endregion vars

        #region widgets
        [UI] private MenuItem mnuFileOpen = null;
        [UI] private MenuItem mnuFileSave = null;
        [UI] private Label lblAppInfo = null;
        [UI] private Button btnLoad = null;
        [UI] private Button btnSave = null;
        [UI] private CheckButton chkVideoEn = null;
        [UI] private CheckButton chkAudioEn = null;
        [UI] private CheckButton chkSubEn = null;
        [UI] private TextBuffer tbLog = null;
        [UI] private TextBuffer tbFiles = null;
        [UI] private ScrolledWindow swLog = null;
        [UI] private ScrolledWindow swFiles = null;
        [UI] private ListStore lsVideo = null;
        [UI] private ListStore lsAudio = null;
        [UI] private ListStore lsSub = null;
        [UI] private TreeView tvVideo = null;
        [UI] private TreeView tvAudio = null;
        [UI] private TreeView tvSub = null;
        [UI] private TreeSelection tvVideoSel = null;
        [UI] private TreeSelection tvAudioSel = null;
        [UI] private TreeSelection tvSubSel = null;
        [UI] private TreeViewColumn tvVideoCx1 = null;
        [UI] private TreeViewColumn tvVideoCx2 = null;
        [UI] private TreeViewColumn tvVideoCx3 = null;
        [UI] private TreeViewColumn tvAudioCx1 = null;
        [UI] private TreeViewColumn tvAudioCx2 = null;
        [UI] private TreeViewColumn tvAudioCx3 = null;
        [UI] private TreeViewColumn tvAudioCx4 = null;
        [UI] private TreeViewColumn tvSubCx1 = null;
        [UI] private TreeViewColumn tvSubCx2 = null;
        [UI] private TreeViewColumn tvSubCx3 = null;
        [UI] private CellRenderer tvVideoCr1 = null;
        [UI] private CellRenderer tvVideoCr2 = null;
        [UI] private CellRenderer tvVideoCr3 = null;
        [UI] private CellRenderer tvAudioCr1 = null;
        [UI] private CellRenderer tvAudioCr2 = null;
        [UI] private CellRenderer tvAudioCr3 = null;
        [UI] private CellRenderer tvAudioCr4 = null;
        [UI] private CellRenderer tvSubCr1 = null;
        [UI] private CellRenderer tvSubCr2 = null;
        [UI] private CellRenderer tvSubCr3 = null;
        [UI] private ProgressBar prbLoadSave = null;
        #endregion widgets

        #region init
        public frmMain(DefaultTrackChanger _dtc) : this(_dtc, new Builder("frmMain.glade")) { }

        private frmMain(DefaultTrackChanger _dtc, Builder builder) : base(builder.GetObject("frmMain").Handle)
        {
            // Theme related code, might implenment in the future. maybe.
            // if (Environment.OSVersion.ToString().Contains("Windows"))
            // {
            //     // Get the Global Settings
            //     var setts = Gtk.Settings.Default;
            //     // This enables clear text on Win32, makes the text look a lot less crappy
            //     setts.XftRgba = "rgb";
            //     // This enlarges the size of the controls based on the dpi
            //     setts.XftDpi = 96;
            //     // By Default Anti-aliasing is enabled, if you want to disable it for any reason set this value to 0
            //     //setts.XftAntialias = 0
            //     // Enable text hinting
            //     setts.XftHinting = 1;
            //     //setts.XftHintstyle = "hintslight"
            //     setts.XftHintstyle = "hintfull";
            // }
            // char sl = System.IO.Path.DirectorySeparatorChar;
            // CssProvider cssprov = new CssProvider();
            // cssprov.LoadFromPath($@"themes{sl}Breeze-Dark{sl}gtk-3.0{sl}gtk.css");
            // StyleContext.AddProviderForScreen(Gdk.Screen.Default, cssprov, StyleProviderPriority.User);

            // Setup app core, subscribe the ui to its events
            dtc = _dtc;
            dtc.OnError += dtc_OnError;
            dtc.FilesAllLoading += dtc_FilesAllLoading;
            dtc.FileLoaded += dtc_FileLoaded;
            dtc.FilesAllLoaded += dtc_FilesAllLoaded;
            dtc.FilesAllSaving += dtc_FilesAllSaving;
            dtc.FileSaved += dtc_FileSaved;
            dtc.FilesAllSaved += dtc_FilesAllSaved;

            // Create UI
            builder.Autoconnect(this);
            DeleteEvent += Window_DeleteEvent;

            // Setup file drag'n'drop into main window
            // https://stackoverflow.com/questions/14273471/drag-and-drop-files-into-program
            Gtk.TargetEntry [  ] target_table = // media type we'll accept
                new TargetEntry [  ] {
                new TargetEntry ("text/uri-list", 0, 0),
                new TargetEntry ("application/x-monkey", 0, 1),
            };
            Gtk.Drag.DestSet (this, DestDefaults.All, target_table, Gdk.DragAction.Copy);

            // Display application information on screen
            lblAppInfo.Text = $"{Assembly.GetExecutingAssembly().GetName().Name} ver{Assembly.GetExecutingAssembly().GetName().Version.ToString()} by damysteryman";      

            // Init track data containers
            InitTreeViews();
            // Init track edit on/off switches
            dtc.VideoEditEnabled = chkVideoEn.Active;
            dtc.AudioEditEnabled = chkAudioEn.Active;
            dtc.SubtitleEditEnabled = chkSubEn.Active;
            // ...and now program is loaded and ready, set status specifying so.
            prbLoadSave.Text = "Program Loaded.";

            // if necessary programs are not installed, stop user from doing stuff and notify them about the issue
            if(!MKVToolNix.IsInstalled())
            {
                UnlockFormControls(false);
                FoundNotify(false);
            }
        }
        #endregion init

        #region callbacks
        private void Window_DeleteEvent(object sender, DeleteEventArgs a) => Application.Quit();
        private async void on_frmMainWindow_drag_data_received(object sender, DragDataReceivedArgs e) => await LoadDragDrop(e.SelectionData.Uris);
        private async void on_btnLoad_activate(object sender, EventArgs e) => await LoadOpenDialog();
        private async void on_btnSave_activate(object sender, EventArgs e) => await Save();
        private async void on_mnuFileOpen_activate(object sender, EventArgs e) => await LoadOpenDialog();
        private async void on_mnuFileSave_activate(object sender, EventArgs e) => await Save();
        private void on_mnuFileExit_activate(object sender, EventArgs e) => Application.Quit();
        private void on_mnuMKVTHome_activate(object sender, EventArgs e) => Utils.OpenURL(MKVToolNix.urlHome);
        private void on_mnuMKVTDownload_activate(object sender, EventArgs e) => Utils.OpenURL(MKVToolNix.urlDownload);
        private void on_mnuMKVTCheck_activate(object sender, EventArgs e)
        {
            bool installed = MKVToolNix.IsInstalled();      // Check if MKVToolNix is installed

            UnlockFormControls(installed);                  // Lock or Unlock certain form controls depending on result of above
            FoundNotify(installed);              // Display message to user depending on result of above
        }
        private void on_tvVideoSel_changed(object sender, EventArgs e)
        {
            if (!dtc.FilesSaving)
                dtc.DefaultVideoIdx = TreeSelectGetIdx(tvVideoSel);
        }
        private void on_tvAudioSel_changed(object sender, EventArgs e)
        {
            if (!dtc.FilesSaving)
                dtc.DefaultAudioIdx = TreeSelectGetIdx(tvAudioSel);
        }
        private void on_tvSubSel_changed(object sender, EventArgs e)
        {
            if (!dtc.FilesSaving)
                dtc.DefaultSubIdx = TreeSelectGetIdx(tvSubSel);
        }
        private void on_chkVideoEn_toggled(object sender, EventArgs e) => dtc.VideoEditEnabled = chkVideoEn.Active;
        private void on_chkAudioEn_toggled(object sender, EventArgs e) => dtc.AudioEditEnabled = chkAudioEn.Active;
        private void on_chkSubEn_toggled(object sender, EventArgs e) => dtc.SubtitleEditEnabled = chkSubEn.Active;
        
        /// <summary>
        /// Callback for DefaultTrackChanger.OnError event.
        /// </summary>
        private void dtc_OnError(object sender, MsgArgs e) => tbLog.Text += (e.Message + Environment.NewLine);
        
        /// <summary>
        /// Callback for DefaultTrackChanger.FileAllLoaded event.
        /// </summary>
        private void dtc_FilesAllLoading(object sender, EventArgs e)
        {
            // Lock form controls from user, and reset File display and progress bar
            UnlockFormControls(false);
            UpdateStatus("Loading Files...");
            tbFiles.Text = "";
            prbLoadSave.Fraction = 0;
            fileLoadSaveCount = 0;
        }

        /// <summary>
        /// Callback for DefaultTrackChanger.FileLoaded event.
        /// </summary>
        private void dtc_FileLoaded(object sender, MsgArgs e)
        {
            // Need to tell UI thread to run update, else GTK will crash
            Application.Invoke(delegate
            {
                // Update progress bar, File display and Log
                fileLoadSaveCount++;
                prbLoadSave.Fraction = (double)fileLoadSaveCount/(double)(dtc.FileCount);
                UpdateTextBuffer(tbFiles, swFiles, e.Message, false);
                UpdateStatus($"File {fileLoadSaveCount} of {dtc.FileCount} Loaded. [{e.Message}]");
            });
        }

        /// <summary>
        /// Callback for DefaultTrackChanger.FilesAllLoaded event.
        /// </summary>
        private void dtc_FilesAllLoaded(object sender, EventArgs e)
        {
            // Unlock form controls when done
            UpdateStatus("Files Loaded.");
            fileLoadSaveCount = 0;
            UnlockFormControls(true);
        }

        /// <summary>
        /// Callback for DefaultTrackChanger.FilesAllSaving event.
        /// </summary>
        private void dtc_FilesAllSaving(object sender, EventArgs e)
        {
            // Lock form controls from user, and reset progress bar
            UnlockFormControls(false);
            UpdateStatus("Saving Files...");
            prbLoadSave.Fraction = 0;
            fileLoadSaveCount = 0;
        }

        /// <summary>
        /// Callback for DefaultTrackChanger.FileSaved event.
        /// </summary>
        private void dtc_FileSaved(object sender, MsgArgs e)
        {
            // Need to tell UI thread to run update, else GTK will crash
            Application.Invoke(delegate
            {
                // Update progress bar and Log
                fileLoadSaveCount++;
                prbLoadSave.Fraction = (double)fileLoadSaveCount/(double)(dtc.FileCount);
                UpdateStatus($"Attempt Save of File {fileLoadSaveCount} of {dtc.FileCount}...");
                UpdateTextBuffer(tbLog, swLog, e.Message, false);
            });
        }

        /// <summary>
        /// Callback for DefaultTrackChanger.FilesAllSaved event.
        /// </summary>
        private void dtc_FilesAllSaved(object sender, EventArgs e)
        {
            // Unlock form controls when done and display message telling user how many files were successfully modified
            string message = $"Saving Complete. {dtc.FilesModified} of {dtc.FileCount} files successfully modified!";
            UpdateStatus(message);
            MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info, ButtonsType.Ok, message);
            md.Run();
            md.Dispose();
            UnlockFormControls(true);
        }
        #endregion callbacks

        #region methods

        /// <summary>
        /// Get the index of the selected item in a Track list's TreeView.
        /// </summary>
        /// <param name="ts">The TreeSelection object corresponding to a TreeView.</param>
        private int TreeSelectGetIdx(TreeSelection ts)
        {
            int idx = 0;
            TreeIter iter;
            ITreeModel model;
            if (ts.GetSelected(out model, out iter))
                idx = model.GetPath(iter).Indices[0];
            return idx;
        }

        /// <summary>
        /// Unlock or Lock various user-interactable onscreen controls to either allow or restrict the user from performing actions.
        /// </summary>
        /// <param name="unlock">true to unlock, false to lock.</param>
        private void UnlockFormControls(bool unlocked)
        {
            mnuFileOpen.Sensitive = unlocked;
            mnuFileSave.Sensitive = unlocked;
            btnLoad.Sensitive = unlocked;
            btnSave.Sensitive = unlocked;
            tvVideo.Selection.Mode = unlocked ? SelectionMode.Single : SelectionMode.None;
            tvAudio.Selection.Mode = unlocked ? SelectionMode.Single : SelectionMode.None;
            tvSub.Selection.Mode = unlocked ? SelectionMode.Single : SelectionMode.None;
        }

        /// <summary>
        /// Asks the user to select MKV files to load (using FileChooser) and load them into application.
        /// </summary>
        private async Task LoadOpenDialog()
        {
            // Create File Chooser Dialog to select files to load up
            Gtk.FileChooserDialog fcd = new Gtk.FileChooserDialog ("Open File", null, Gtk.FileChooserAction.Open);
			fcd.AddButton (Gtk.Stock.Cancel, Gtk.ResponseType.Cancel);
			fcd.AddButton (Gtk.Stock.Open, Gtk.ResponseType.Ok);
			fcd.DefaultResponse = Gtk.ResponseType.Ok;
			fcd.SelectMultiple = true;

            // Setup Dialog filter to show only mkv files
            FileFilter filter  = new FileFilter();
            filter.Name = "Matroska MKV files";
            filter.AddPattern("*.mkv");
            fcd.Filter = filter;

            // Get list of selected files from Dialog
            List<string> files = new List<string>();
			Gtk.ResponseType response = (Gtk.ResponseType)fcd.Run();
			if (response == Gtk.ResponseType.Ok)
                foreach (string f in fcd.Filenames)
                    files.Add(f);
			fcd.Dispose();

            // Load selected files, or display error in log if there are none
            if (files.Count < 1)
                UpdateTextBuffer(tbLog, swLog, "ERROR!: There are no files selected! Nothing to do.", true);
            else
                await Load(files);
        }

        /// <summary>
        /// Loads a list of files from drag'n'drop into the application.
        /// </summary>
        /// <param name="uris">The list of file uris obtained from drag'n'drop event to load into the application.</param>
        public async Task LoadDragDrop(string[] uris)
        {
            // Grab paths of files that were dragged'n'dropped onto UI and load them
            List<string> files = new List<string>();
            foreach (string u in uris)
                files.Add(new Uri(u).LocalPath);

            await Load(files);
        }

        /// <summary>
        /// Loads a list of files into the application and displays data on screen.
        /// </summary>
        /// <param name="files">The list of file paths to load into the application.</param>
        public async Task Load(List<string> files)
        {
            // Empty the UI of any existing track data, load new data, and then display the new data in UI
            ClearLists();
            await dtc.LoadFileInfoAsync(files);
            FillLists();
        }

        /// <summary>
        /// Saves the selected default values to MKV files loaded into application.
        /// </summary>
        private async Task Save()
        {
            // Save the default selected settings to file(s) and refresh default selection in UI
            await dtc.SaveAllFilesAsync();
            SetListDefaults();
        }

        /// <summary>
        /// Appends a text message to a specified output text box while auto-scrolling it to follow the added text.
        /// </summary>
        /// <param name="tb">The TextBuffer corresponding to the text box to update.</param>
        /// <param name="sw">The ScrolledWindow corresponding to the text box to update.</param>
        /// <param name="debug">If true, will also print message to debug console.</param>
        /// <param name="membername">[optional] If debug is true, the method name passed to myDebug.WriteLine().</param>
        public void UpdateTextBuffer(TextBuffer tb, ScrolledWindow sw, string text, bool debug, [System.Runtime.CompilerServices.CallerMemberName] string membername = "")
        {
            // Need to tell UI thread to run update, else GTK will crash
            Application.Invoke(delegate
            {
                tb.Text += text + Environment.NewLine;
                sw.Vadjustment.Value = sw.Vadjustment.Upper - sw.Vadjustment.PageSize;
            });
            if (debug)
                myDebug.WriteLine(text, membername);      
        }

        /// <summary>
        /// Writes a status message to the Progres Bar and to the Log.
        /// </summary>
        /// <param name="status">The status message to display.</param>
        public void UpdateStatus(string status)
        {
            prbLoadSave.Text = status;
            UpdateTextBuffer(tbLog, swLog, status, true);
        }

        /// <summary>
        /// Helper method for TreeCellDataFunc to render a Track's Language data inside a TreeView.
        /// </summary>
        private void RenderTrackLang (Gtk.TreeViewColumn column, Gtk.CellRenderer cell, Gtk.ITreeModel model, Gtk.TreeIter iter)
        {
            Track t = (Track) model.GetValue (iter, 0);
            (cell as Gtk.CellRendererText).Text = t.Language;
        }

        /// <summary>
        /// Helper method for TreeCellDataFunc to render a Track's Format data inside a TreeView.
        /// </summary>
        private void RenderTrackFormat (Gtk.TreeViewColumn column, Gtk.CellRenderer cell, Gtk.ITreeModel model, Gtk.TreeIter iter)
        {
            Track t = (Track) model.GetValue (iter, 0);
            (cell as Gtk.CellRendererText).Text = t.Format;
        }

        /// <summary>
        /// Helper method for TreeCellDataFunc to render a Track's Name data inside a TreeView.
        /// </summary>
        private void RenderTrackName (Gtk.TreeViewColumn column, Gtk.CellRenderer cell, Gtk.ITreeModel model, Gtk.TreeIter iter)
        {
            Track t = (Track) model.GetValue (iter, 0);
            (cell as Gtk.CellRendererText).Text = t.Name;
        }

        /// <summary>
        /// Helper method for TreeCellDataFunc to render an AudioTrack's Channel data inside a TreeView.
        /// </summary>
        private void RenderAudioTrackChannels (Gtk.TreeViewColumn column, Gtk.CellRenderer cell, Gtk.ITreeModel model, Gtk.TreeIter iter)
        {
            AudioTrack t = (AudioTrack) model.GetValue (iter, 0);
            (cell as Gtk.CellRendererText).Text = t.Channels.ToString();
        }

        /// <summary>
        /// Initializes all TreeViews to display data from their respective Track lists.
        /// </summary>
        private void InitTreeViews()
        {
            lsVideo = new ListStore (typeof (VideoTrack));
            tvVideoCx1.SetCellDataFunc(tvVideoCr1, new Gtk.TreeCellDataFunc(RenderTrackLang));
            tvVideoCx2.SetCellDataFunc(tvVideoCr2, new Gtk.TreeCellDataFunc(RenderTrackFormat));
            tvVideoCx3.SetCellDataFunc(tvVideoCr3, new Gtk.TreeCellDataFunc(RenderTrackName));
            tvVideo.Model = lsVideo;

            lsAudio = new ListStore (typeof (AudioTrack));
            tvAudioCx1.SetCellDataFunc(tvAudioCr1, new Gtk.TreeCellDataFunc(RenderTrackLang));
            tvAudioCx2.SetCellDataFunc(tvAudioCr2, new Gtk.TreeCellDataFunc(RenderTrackFormat));
            tvAudioCx3.SetCellDataFunc(tvAudioCr3, new Gtk.TreeCellDataFunc(RenderAudioTrackChannels));
            tvAudioCx4.SetCellDataFunc(tvAudioCr4, new Gtk.TreeCellDataFunc(RenderTrackName));
            tvAudio.Model = lsAudio;

            lsSub = new ListStore (typeof (SubtitleTrack));
            tvSubCx1.SetCellDataFunc(tvSubCr1, new Gtk.TreeCellDataFunc(RenderTrackLang));
            tvSubCx2.SetCellDataFunc(tvSubCr2, new Gtk.TreeCellDataFunc(RenderTrackFormat));
            tvSubCx3.SetCellDataFunc(tvSubCr3, new Gtk.TreeCellDataFunc(RenderTrackName));
            tvSub.Model = lsSub;
        }

        /// <summary>
        /// Clears all data from all ListStores.
        /// </summary>
        private void ClearLists()
        {
            // Clear ListStore contents
            lsVideo.Clear();
            lsAudio.Clear();
            lsSub.Clear();
        }

        /// <summary>
        /// Populates all the ListStores with data from their Respective Track lists and sets the default selection.
        /// </summary>
        private void FillLists()
        {
            // Populate ListStores with data
            foreach (VideoTrack t in dtc.VideoTracks)
                lsVideo.AppendValues(t);
            foreach (AudioTrack t in dtc.AudioTracks)
                lsAudio.AppendValues(t);
            foreach (SubtitleTrack t in dtc.SubTracks)
                lsSub.AppendValues(t);

            SetListDefaults();
        }

        /// <summary>
        /// Set the Default selections on all TreeViews to match the default index in their respective Track lists.
        /// </summary>
        private void SetListDefaults()
        {
            // Set default selections
            TreeIter iter;
            if (lsVideo.GetIter(out iter, new TreePath(new int[] { dtc.DefaultVideoIdx })))
                tvVideo.Selection.SelectIter(iter);   
            if (lsAudio.GetIter(out iter, new TreePath(new int[] { dtc.DefaultAudioIdx })))
                tvAudio.Selection.SelectIter(iter);
            if (lsSub.GetIter(out iter, new TreePath(new int[] { dtc.DefaultSubIdx })))
                tvSub.Selection.SelectIter(iter);
        }
        /// <summary>
        /// Notifiy the user with a message box regarding whether or not MKVToolNix was found on their system.
        /// </summary>
        /// <param name="found">true if MKVToolNix was found, false otherwise.</param>
        public void FoundNotify(bool found)
        {
            if (!found)
            {
                // Inform user they do not have it installed and ask if they want to visit download page for it
                string message = "MKVToolNix does not seem to be installed on this machine, but it is needed for this program to function." +
                                Environment.NewLine + Environment.NewLine +
                                "Would you like to go to MKVToolNix's download page?";
                MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Question, ButtonsType.YesNo, message);
                if ((ResponseType)md.Run() == ResponseType.Yes)
                    Utils.OpenURL(MKVToolNix.urlDownload);
                md.Dispose();
            }
            else
            {
                // Show message box informing the user that MKVToolNix was found already installed
                string message = "Looks like MKVToolnix is installed on this machine, so you should be good to go!";
                MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info, ButtonsType.Ok, message);
                md.Run();
                md.Dispose();
            }   
        }
        #endregion methods
    }
}
