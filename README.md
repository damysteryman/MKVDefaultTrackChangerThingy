# MKVDefaultTrackChangerThingy v2.0 MultiPlatform (GTK) Version
A tool to easily change the default Audio and Subtitle tracks in one or more MKV files that all have same or similar track configuration (Example: entire season of TV show). Uses for this include quickly changing from main tracks to commentary and vice-versa for a TV series, or for quickly switching video/audio/subtitle languages for foreign TV shows.


v2.0 is a port/rewrite of the program of the same name for multiplatform (Linux + Windows) support, by porting the GUI from WinForms to GTK using GtkSharp+Glade and .NET Core, along with a major code refactor, separating the UI and core logic, and other minor cleanup.


NOTE: Requires the MKVToolNix software tools installed on your machine to function! The portable version located in "mkvtoolnix" subfolder in the same folder as this app will work too. Get it here: [https://mkvtoolnix.download/downloads.html]


There are no plans to port MKVTrackNamerThingy or MKVBatchTitleNamerThingy at this time.
# Main components:
## MKVDefaultTrackChanger
The core of the application. Handles MKV file parsing and editing.

## MKVDefaultTrackChangerThingy
The application's UI. Used to interact with MKVDefaultTrackChanger core. Hands it data based on user action while receiving data via events.

## MKVMetaThingyUtils
A small library dll with some shared utility code, that is all.
