/*
 * Copyright (C) 2016-2020  damysteryman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using MKVMetaThingyUtils;
using System;
using System.Collections.Generic;

namespace MKVDefaultTrackChanger
{
    public abstract class Track
    {
        public string Name { get; set; }
        public string Language { get; set; }
        public string Format { get; set; }
        public bool IsDefault { get; set; }

        public Track()
        {
            Name = "(none)";
            Language = "(none)";
            Format = "(none)";
            IsDefault = false;
        }

        public Track(string name, string lang, string format, bool isDefault)
        {
            Name = name;
            Language = lang;
            Format = format;
            IsDefault = isDefault;
        }

        protected string BuildDefaultChangeArgs(bool addSetting, int idx, string type)
        {
            return " -e track:" + type + idx + " -" + (addSetting ? "a" : "s") + " flag-default=" + Convert.ToInt16(IsDefault);
        }
    }

    public class VideoTrack : Track
    {
        public VideoTrack() : base() { }
        public VideoTrack(string name, string lang, string format, bool isDefault) : base(name, lang, format, isDefault) { }
        public string BuildDefaultChangeArgs(int idx, bool addSetting = false) => base.BuildDefaultChangeArgs(addSetting, idx, "v");
    }

    public class AudioTrack : Track
    {
        public int Channels { get; set; }

        public AudioTrack() : base()
        {
            Channels = 0;
        }
        public AudioTrack(string name, string lang, string format, bool isDefault, int channels) : base(name, lang, format, isDefault)
        {
            Channels = channels;
        }
        public string BuildDefaultChangeArgs(int idx, bool addSetting = false) => base.BuildDefaultChangeArgs(addSetting, idx, "a");
    }

    public class SubtitleTrack : Track
    {
        public SubtitleTrack() : base() { }
        public SubtitleTrack(string name, string lang, string format, bool isDefault) : base(name, lang, format, isDefault) { }
        public string BuildDefaultChangeArgs(int idx, bool addSetting = false) => base.BuildDefaultChangeArgs(addSetting, idx, "s");
    }

    public class InvalidTrackTypeException : Exception
    {
        public InvalidTrackTypeException(string message) : base(message) { }
    }

    public static class TrackFactory
    {
        public static Track MakeTrack(dynamic json)
        {
            string type = json["type"];

            string name;
            bool isDefault;

            try
            {
                name = json["properties"]["track_name"];      // Try to get the track name from dynamic object
            }
            catch (KeyNotFoundException)
            {
                myDebug.WriteLine("Track has no name... using blank name instead.");
                name = "";                                                 // If it is not even present in object, use blank name instead
            }

            try
            {
                isDefault = json["properties"]["default_track"];  // Try to get the default track flag value from dynamic object
            }
            catch (KeyNotFoundException)
            {
                myDebug.WriteLine("Track's \"Default Track\" flag does not exist... setting default track flag to false.");
                isDefault = false;                                         // If it is not even present in object, use default of false
            }

            string lang = ((string)json["properties"]["language"]).ToUpper();   // Get track language code from dynamic object
            string format = json["codec"];

            switch (type)
            {
                case MKVToolNix.TRACK_VIDEO:
                    return new VideoTrack(name, lang, format, isDefault);

                case MKVToolNix.TRACK_AUDIO:
                    int channels = json["properties"]["audio_channels"];
                    return new AudioTrack(name, lang, format, isDefault, channels);

                case MKVToolNix.TRACK_SUB:
                    return new SubtitleTrack(name, lang, format, isDefault);

                default:
                    string message = "Invalid/Unsupported track type detected!" + Environment.NewLine + $"Track says it is of type \"{ type }\".";
                    throw new InvalidTrackTypeException(message);
            }
        }
    }
}
