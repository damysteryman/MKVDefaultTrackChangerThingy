﻿/*
 * Copyright (C) 2016-2020  damysteryman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

using MKVMetaThingyUtils;

namespace MKVDefaultTrackChanger
{
    public class MsgArgs : EventArgs
    {
        public string Message { get; private set; }
        public MsgArgs(string _msg) => Message = _msg;
    }

    public class FileLoadArgs : EventArgs
    {
        public string Path { get; private set; }
        public FileLoadArgs(string _path) => Path = _path;
    }

    public class DefaultTrackChanger
    {
        #region vars
        private int maxVideoIndex;              // Initialize index of file holding the most video tracks
        private int maxAudioIndex;              // Initialize index of file holding the most audio tracks
        private int maxSubIndex;                // Initialize index of file holding the most subtitle tracks
        private int[] videoCounts;
        private int[] audioCounts;
        private int[] subCounts;
        #endregion vars
        
        #region properties
        public string[] MkvFiles { get; private set; }
        public bool FilesLoading { get; private set; }
        public bool FilesSaving { get; private set; }
        public int FileCount { get { return MkvFiles.Length; }}
        public int FilesModified { get; private set; }              // Initialize a counter to count the amount of files successfully modified
        public List<VideoTrack> VideoTracks { get; private set; }
        public List<AudioTrack> AudioTracks { get; private set; }
        public List<SubtitleTrack> SubTracks { get; private set; }
        public int DefaultVideoIdx { get { return GetDefault(VideoTracks); } set { SetDefault(VideoTracks, value); }}
        public int DefaultAudioIdx { get { return GetDefault(AudioTracks); } set { SetDefault(AudioTracks, value); }}
        public int DefaultSubIdx { get { return GetDefault(SubTracks); } set { SetDefault(SubTracks, value); }}
        public bool VideoEditEnabled { get; set; }
        public bool AudioEditEnabled { get; set; }
        public bool SubtitleEditEnabled { get; set; }
        #endregion properties

        #region events
        public event EventHandler<MsgArgs> OnError;
        public event EventHandler FilesAllLoading;
        public event EventHandler<MsgArgs> FileLoaded;
        public event EventHandler FilesAllLoaded;
        public event EventHandler FilesAllSaving;
        public event EventHandler<MsgArgs> FileSaved;
        public event EventHandler FilesAllSaved;
        #endregion events

        #region init
        /// <summary>
        /// Main constructor
        /// </summary>
        public DefaultTrackChanger()
        {
            FilesModified = 0;              // Initialize a counter to count the amount of files successfully modified
            maxVideoIndex = 0;              // Initialize index of file holding the most video tracks
            maxAudioIndex = 0;              // Initialize index of file holding the most audio tracks
            maxSubIndex = 0;                // Initialize index of file holding the most subtitle tracks
            videoCounts = new int[0];
            audioCounts = new int[0];
            subCounts = new int[0];
            MkvFiles = new string[0];
            VideoTracks = new List<VideoTrack>() { new VideoTrack() };
            AudioTracks = new List<AudioTrack>() { new AudioTrack() };
            SubTracks = new List<SubtitleTrack>() { new SubtitleTrack() };
        }
        #endregion init

        #region methods
        private int GetDefault(IEnumerable<Track> tlist)
        {
            int value = 0;
            int cnt = 0;
            foreach (Track t in tlist)
            {
                if (t.IsDefault)
                    value = cnt;
                cnt++;
            }
            return value;
        }
        private void SetDefault(IEnumerable<Track> tlist, int value)
        {
            int cnt = 0;
            foreach (Track t in tlist)
            {
                if (cnt == value)
                    t.IsDefault = true;
                else
                    t.IsDefault = false;
                cnt++;
            }
        }
        private IList GetTracks(dynamic json, string type = "")
        {
            IList tracks = new List<Track>();

            switch (type)
            {
                case MKVToolNix.TRACK_VIDEO:
                    tracks = new List<VideoTrack>();
                    break;

                case MKVToolNix.TRACK_AUDIO:
                    tracks = new List<AudioTrack>();
                    break;

                case MKVToolNix.TRACK_SUB:
                    tracks = new List<SubtitleTrack>();
                    break;

                default: // if track type is unsupported/not specified, just get and retun all of them
                    for (int i = 0; i < json["tracks"].Count; i++)
                        tracks.Add(TrackFactory.MakeTrack(json["tracks"][i]));
                    return tracks;
            }   

            for (int i = 0; i < json["tracks"].Count; i++)
            {
                if (json["tracks"][i]["type"] == type)
                    tracks.Add(TrackFactory.MakeTrack(json["tracks"][i]));
            }

            return tracks;
        }

        private void LoadFileTrackCounts(int fileIndex, string path, string exeName)
        {
            // Set the dynamic json object to...
            // ...the output derived from the JSON string...
            // ...which is taken from the output from mkvmerge.exe
            dynamic json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + path + "\""));
            FileLoaded(this, new MsgArgs(path));
            int videoCount = 0;
            int audioCount = 0;                             // Initialize audio track counter
            int subCount = 0;                               // Initialize subtitle track counter

            for (int j = 0; j < json["tracks"].Count; j++)         // For each track in the file...
            {
                switch ((string)json["tracks"][j]["type"])          // Check the track type
                {
                    case MKVToolNix.TRACK_VIDEO:
                        videoCount++;
                        break;

                    case MKVToolNix.TRACK_AUDIO:                    // If it is an audio track
                        audioCount++;                               // Increment audio track counter
                        break;                                      // Break out of switch statement

                    case MKVToolNix.TRACK_SUB:                      // If it is a subtitle track
                        subCount++;                                 // Increment subtitle track counter
                        break;                                      // Break out of switch statement
                }
            }
            
            videoCounts[fileIndex] = videoCount;
            audioCounts[fileIndex] = audioCount;
            subCounts[fileIndex] = subCount;

            myDebug.WriteLine($"{Path.GetFileName(path)}:\thas {videoCount} video, {audioCount} audio, and {subCount} subs.");
        }

        /// <summary>
        /// Method to load data from selected files
        /// </summary>
        /// <param name="MkvFiles">Array of selected files' paths</param>
        public async Task LoadFileInfoAsync(List<string> _mkvFiles)
        {
            foreach (string f in _mkvFiles)
                if (!f.EndsWith(".mkv"))
                {
                    string msg = "ERROR!: Selected file is not an .mkv file! Skipping... (" + f + ")";
                    myDebug.WriteLine(msg);
                    OnError(this, new MsgArgs(msg));
                }
            _mkvFiles.RemoveAll(s => !s.EndsWith(".mkv"));
            MkvFiles = _mkvFiles.ToArray();

            string exeName = MKVToolNix.ExeGetPath(MKVToolNix.mkvmergeExe);
            dynamic json = null;

            maxVideoIndex = 0;
            maxAudioIndex = 0;                              // Initialize index of file holding the most audio tracks 
            maxSubIndex = 0;                                // Initialize index of file holding the most subtitle tracks

            videoCounts = new int[MkvFiles.Length];
            audioCounts = new int[MkvFiles.Length];
            subCounts = new int[MkvFiles.Length];

            List<Task> fileLoaders = new List<Task>();

            FilesLoading = true;
            FilesAllLoading(this, new EventArgs());
            for (int i = 0; i < MkvFiles.Length; i++)           // For each file in the directory...
            {
                int idx = i;
                string path = MkvFiles[idx];
                fileLoaders.Add(Task.Run(() => LoadFileTrackCounts(idx, path, exeName)));
            }
            await Task.WhenAll(fileLoaders);
            FilesAllLoaded(this, new EventArgs());
            FilesLoading = false;

            maxVideoIndex = Array.IndexOf(videoCounts, videoCounts.Max());
            maxAudioIndex = Array.IndexOf(audioCounts, audioCounts.Max());
            maxSubIndex = Array.IndexOf(subCounts, subCounts.Max());

            VideoTracks = new List<VideoTrack>() { new VideoTrack() };
            AudioTracks = new List<AudioTrack>() { new AudioTrack() };
            SubTracks = new List<SubtitleTrack>() { new SubtitleTrack() };

            myDebug.WriteLine($"VIDEO:\t{ maxVideoIndex }:{ videoCounts[maxVideoIndex] }");
            myDebug.WriteLine($"AUDIO:\t{ maxAudioIndex }:{ audioCounts[maxAudioIndex] }");
            myDebug.WriteLine($"SUB  :\t{ maxSubIndex }:{ subCounts[maxSubIndex] }");

            if (maxAudioIndex == maxVideoIndex && maxAudioIndex == maxSubIndex) // best case, all same
            {
                // Get dynamic object from JSON string from mkvmerge.exe for file in folder with max amount of audio tracks
                json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + MkvFiles[maxAudioIndex] + "\""));
                VideoTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_VIDEO));
                AudioTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_AUDIO));
                SubTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_SUB));
            }
            else
            {
                if (maxAudioIndex == maxSubIndex) // 2/3 case, video different
                {
                    json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + MkvFiles[maxAudioIndex] + "\""));
                    AudioTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_AUDIO));
                    SubTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_SUB));

                    json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + MkvFiles[maxVideoIndex] + "\""));
                    VideoTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_VIDEO));
                }
                else if (maxVideoIndex == maxSubIndex) // 2/3 case, audio different
                {
                    json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + MkvFiles[maxVideoIndex] + "\""));
                    VideoTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_VIDEO));
                    SubTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_SUB));

                    json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + MkvFiles[maxAudioIndex] + "\""));
                    AudioTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_AUDIO));
                }
                else if (maxAudioIndex == maxVideoIndex) // 2/3 case, sub different
                {
                    json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + MkvFiles[maxAudioIndex] + "\""));
                    AudioTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_AUDIO));
                    VideoTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_VIDEO));

                    json = Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + MkvFiles[maxSubIndex] + "\""));
                    SubTracks.AddRange(GetTracks(json, MKVToolNix.TRACK_SUB));
                }
                else // worst case, all different
                {
                    VideoTracks.AddRange(GetTracks(Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + MkvFiles[maxVideoIndex] + "\"")), MKVToolNix.TRACK_VIDEO));
                    AudioTracks.AddRange(GetTracks(Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + MkvFiles[maxAudioIndex] + "\"")), MKVToolNix.TRACK_AUDIO));
                    SubTracks.AddRange(GetTracks(Utils.GetJsonObject(Utils.RunProcess(exeName, "-i -F json \"" + MkvFiles[maxSubIndex] + "\"")), MKVToolNix.TRACK_SUB));
                }
            }
        }


        /// <summary>
        /// Method to save the currently selected default track configuration to file(s)
        /// </summary>
        public async Task SaveAllFilesAsync()
        {
            if (MkvFiles.Length > 0)
            {
                if (VideoEditEnabled || AudioEditEnabled || SubtitleEditEnabled)
                {
                    FilesSaving = true;
                    FilesAllSaving(this, new EventArgs());
                    string exeName = MKVToolNix.ExeGetPath(MKVToolNix.mkvpropeditExe);    // Initialize name for exe for Utils.RunProcess, grabbing path for currently installed mkvpropedit.exe and wrapping in quotes

                    StringBuilder args = new StringBuilder();       // Instantiate new StringBuilder to construct final arguments string

                    if (VideoEditEnabled)
                        for (int i = 1; i < VideoTracks.Count; i++)
                            args.Append(VideoTracks[i].BuildDefaultChangeArgs(i));
                    if (AudioEditEnabled)
                        for (int i = 1; i < AudioTracks.Count; i++)
                            args.Append(AudioTracks[i].BuildDefaultChangeArgs(i));
                    if (SubtitleEditEnabled)
                        for (int i = 1; i < SubTracks.Count; i++)
                            args.Append(SubTracks[i].BuildDefaultChangeArgs(i));

                    FilesModified = 0;                                              // Initialize counter to count how many files were successfully modified
                    List<Task<string>> filesSaving = new List<Task<string>>();

                    for (int i = 0; i < MkvFiles.Length; i++)                            // For each file...
                    {   
                        int idx = i;
                        filesSaving.Add(Task.Run(() => SaveFile(MkvFiles[idx], args.ToString(), exeName, idx, MkvFiles.Length)));
                    }
                    await Task.WhenAll(filesSaving);
                    FilesAllSaved(this, new EventArgs());
                    FilesSaving = false;
                }
                else
                {
                    OnError(this, new MsgArgs("ERROR!: Video, Audio, and Subtitle Editing are all disabled! Nothing to do."));
                }
            }
            else
            {
                OnError(this, new MsgArgs("ERROR!: There are no files loaded! Nothing to do."));
            }
        }

        private string SaveFile(string path, string args, string exeName, int idx, int count)
        {
            StringBuilder result = new StringBuilder();
            string retStr = "";
            string fileName = "\"" + path + "\"";    // Wrap file name in quotes for use in args

            myDebug.WriteLine(path);
            result.AppendLine($"File: {path}"); // Send to log file no. and name
            result.AppendLine($"{exeName} {fileName}{args}");   // Send to log the complete command sent to mkvpropedit
            retStr = Utils.RunProcess(exeName, fileName + args.ToString()); // Run mkvpropedit with final generated args
            result.AppendLine(retStr);                       // send returned result from mkvpropedit to log
            myDebug.WriteLine(result.ToString());
            int goodVideoCount = VideoTracks.Count;                       // Initialize count of "good" video, count of subtitle tracks found in file
            int goodAudioCount = AudioTracks.Count;                       // Initialize count of "good" audio, count of audio tracks found in file (needed for batch mode, may not have all tracks in selected file) 
            int goodSubCount = SubTracks.Count;                      // Initialize count of "good" subtitle, count of subtitle tracks found in file
            StringBuilder argsAlt;                                          // Declare another StringBuilder for an alternate final arg (used for batch mode, in case some tracks are not found)

            FilesModified++;                                                // Increment filesModified

            while (retStr != MKVToolNix.mkvpropedit_OK)               // While mkvpropedit does not return the "everything completed OK" result
            {
                myDebug.WriteLine(result.ToString());
                if (retStr.Contains(MKVToolNix.mkvpropedit_ERROR_CannotOpenFile))
                {
                    result.AppendLine("ERROR: could not seem to access this file, skipping it..." + Environment.NewLine);
                    FilesModified--;
                    break;
                }

                if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingVideoTrack))
                    goodVideoCount--;
                else if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingAudioTrack))          // If returned error was an audio track not found...
                    goodAudioCount--;                                       // Means the last audio track was not found, so decrement good audio count
                else if (retStr.Contains(MKVToolNix.mkvPropedit_ERROR_MissingSubTrack))     // If returned error was a subtitle track not found...
                    goodSubCount--;                                         // Means the last subtitle track was not found, so decrement good subtitle count

                if (goodVideoCount < DefaultVideoIdx || goodAudioCount < DefaultAudioIdx || goodSubCount < DefaultSubIdx)    // If the selected default tracks are not in the "good" tracks avaible in file...
                {
                    result.AppendLine("Default track selection does not exist in this file, skipping file...");      // inform user that their selected tracks are not present
                    result.AppendLine();
                    FilesModified--;                        // decrement filesModified
                    break;                                  // break out of while loop, skipping file
                }

                argsAlt = new StringBuilder();              // Instantiate new StringBuilder for alternate args
                if (VideoEditEnabled)
                    for (int i = 1; i < goodVideoCount; i++)
                        argsAlt.Append(VideoTracks[i].BuildDefaultChangeArgs(i));
                if (AudioEditEnabled)
                    for (int i = 1; i < goodAudioCount; i++)
                        argsAlt.Append(AudioTracks[i].BuildDefaultChangeArgs(i));
                if (SubtitleEditEnabled)
                    for (int i = 1; i < goodSubCount; i++)
                        argsAlt.Append(SubTracks[i].BuildDefaultChangeArgs(i));

                result.AppendLine($"{exeName} {fileName}{argsAlt.ToString()}");    // Send new complete command to log
                retStr = Utils.RunProcess(exeName, fileName + argsAlt.ToString());  // Try running mkvpropedit with new args
                result.AppendLine(retStr);                                           // send result from mkvpropedit to log
            }
            FileSaved(this, new MsgArgs(result.ToString()));
            return result.ToString();
        }
        #endregion methods
    }
}
